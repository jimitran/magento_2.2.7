<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Controller\Quote\Request;

/**
 * Class Quickquote
 * @package Cart2Quote\Quotation\Controller\Quote\Request
 */
class QuickQuote extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var \Magento\Customer\Model\Session\Proxy
     */
    private $customerSession;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\CreateQuote
     */
    private $createQuote;
    /**
     * @var \Cart2Quote\Quotation\Model\QuoteFactory
     */
    private $quotationFactory;
    /**
     * @var \Cart2Quote\Quotation\Model\Session\Proxy
     */
    private $quoteSession;
    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formKeyValidator;
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Email\Sender\QuoteRequestSender
     */
    private $sender;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Quickquote constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\Session\Proxy $customerSession
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Cart2Quote\Quotation\Model\Quote\Email\Sender\QuoteRequestSender $sender
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Cart2Quote\Quotation\Model\Session\Proxy $quoteSession
     * @param \Cart2Quote\Quotation\Model\QuoteFactory $quotationFactory
     * @param \Cart2Quote\Quotation\Model\Quote\CreateQuote $createQuote
     * @param \Magento\Checkout\Model\Session\Proxy $checkoutSession
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\Session\Proxy $customerSession,
        \Psr\Log\LoggerInterface $logger,
        \Cart2Quote\Quotation\Model\Quote\Email\Sender\QuoteRequestSender $sender,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Cart2Quote\Quotation\Model\Session\Proxy $quoteSession,
        \Cart2Quote\Quotation\Model\QuoteFactory $quotationFactory,
        \Cart2Quote\Quotation\Model\Quote\CreateQuote $createQuote,
        \Magento\Checkout\Model\Session\Proxy $checkoutSession,
        \Magento\Framework\App\Action\Context $context
    ) {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->createQuote = $createQuote;
        $this->quotationFactory = $quotationFactory;
        $this->quoteSession = $quoteSession;
        $this->formKeyValidator = $formKeyValidator;
        $this->sender = $sender;
        $this->logger = $logger;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setRefererUrl();
        }
        try {
            $customerNote = $this->getRequest()->getParam('remarks', null);
            $quote = $this->createQuote->getQuote();
            if ($this->customerSession->isLoggedIn()) {
                $quote->setCustomer($this->customerRepository->getById($this->customerSession->getCustomerId()));
            } else {
                $email = $this->getRequest()->getParam('email', null);
                $firstname = $this->getRequest()->getParam('firstname', null);
                $lastname = $this->getRequest()->getParam('lastname', null);

                if (!isset($email, $firstname, $lastname)) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Missing required parameters.')
                    );
                }

                $quote->setCustomerEmail($email);
                $quote->setCustomerFirstname($firstname);
                $quote->setCustomerLastname($lastname);
                $quote->setCustomerIsGuest(true);
                $quote->setCustomerGroupId(\Magento\Customer\Api\Data\GroupInterface::NOT_LOGGED_IN_ID);
            }

            $quote->setCustomerNote($customerNote);
            $quoteModel = $this->quotationFactory->create();
            $quotation = $quoteModel->create($quote)->load($quote->getId());
            $quotation->saveQuote();
            $this->sender->send($quotation);
            $this->_eventManager->dispatch(
                'quotation_event_after_quote_request',
                ['quote' => $quotation]
            );
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->logger->error($exception);
            $this->messageManager->addErrorMessage(
                __('Something went wrong while processing your quote. Please try again later.')
            );

            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
        }

        return $this->resultRedirectFactory->create()->setUrl(
            $this->_url->getUrl('quotation/quote/success', ['id' => $quotation->getId()])
        );
    }
}