<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Controller\Adminhtml\Product;

/**
 * Class Create
 * @package Cart2Quote\Quotation\Controller\Adminhtml\Product
 */
class Create extends \Magento\Backend\App\Action
{
    /**
     * @const create new product setting value
     */
    const CREATE_NEW_PRODUCT = '1';

    /**
     * @var \Cart2Quote\Quotation\Model\Quote\CustomProduct
     */
    protected $customProduct;

    /**
     * Create constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Cart2Quote\Quotation\Model\Quote\CustomProduct $customProduct
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Cart2Quote\Quotation\Model\Quote\CustomProduct $customProduct
    ) {
        $this->customProduct = $customProduct;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $productParams = $this->getRequest()->getParams();
            if (isset($productParams['setting']) && $productParams['setting'] === self::CREATE_NEW_PRODUCT) {
                $response = $this->customProduct->createNewProduct($productParams);
            } else {
                $response = $this->customProduct->useExistingProduct($productParams);
            }
            $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
            $resultJson->setData($response);

            return $resultJson;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Cart2Quote_Quotation::actions');
    }
}
