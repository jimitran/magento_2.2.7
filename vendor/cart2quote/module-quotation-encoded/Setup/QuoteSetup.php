<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2019] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2019. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Setup;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;

/**
 * Class QuoteSetup
 * Backwards compatible with Magento 2.1 for Commerce Split Database
 * @package Cart2Quote\Quotation\Setup
 */
class QuoteSetup extends \Magento\Quote\Setup\QuoteSetup
{
    /**
     * @var string
     */
    const CONNECTION_NAME = 'checkout';

    /**
     * Get quote connection
     *
     * @return \Magento\Framework\DB\Adapter\AdapterInterfaceode/Cart2Quote/Quotation/Model/ResourceModel/Quote/Status/Collection.php
     */
    public function getConnection()
    {
        if (method_exists(parent::class,'getConnection')) {
            return parent::getConnection();
        }

        return $this->getSetup()->getConnection(self::CONNECTION_NAME);
    }

    /**
     * Get table name
     *
     * @param string $table
     * @return string
     */
    public function getTable($table)
    {
        if (method_exists(parent::class, 'getTable')) {
            return parent::getTable($table);
        }

        return $this->getSetup()->getTable($table, self::CONNECTION_NAME);
    }
}
