/*
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2019] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2019. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

define([
    'jquery',
    'mage/translate',
    'jquery/ui',
    'Magento_Catalog/js/catalog-add-to-cart'
], function ($, $t) {
    "use strict";

    $.widget('cart2quote.catalogAddToCart', $.mage.catalogAddToCart, {
        options: {
            defaultButtonSelector: '',
            defaultButtonDisabledClass: '',
            defaultButtonTextWhileAdding: '',
            defaultButtonTextAdded: '',
            defaultButtonTextDefault: '',
            defaultMiniSelector: '',
            addToQuoteButtonSelector: '.action.toquote',
            addToQuoteButtonDisabledClass: 'disabled',
            addToQuoteButtonTextWhileAdding: $t('Adding to Quote...'),
            addToQuoteButtonTextAdded: $t('Added'),
            addToQuoteButtonTextDefault: $t('Add to Quote'),
            miniquoteSelector: '[data-block="miniquote"]',
            defaultAction: null,
            addToQuoteFormAction: null
        },
        /** @inheritdoc */
        _create: function () {
            this._super();
            this._setDefaultValues();
        },
        /** @protected */
        _setDefaultValues: function () {
            this.options.defaultAction = this.element.attr('action');
            this.options.defaultButtonSelector = this.options.addToCartButtonSelector;
            this.options.defaultButtonDisabledClass = this.options.addToCartButtonDisabledClass;
            this.options.defaultButtonTextWhileAdding = this.options.addToCartButtonTextWhileAdding;
            this.options.defaultButtonTextAdded = this.options.addToCartButtonTextAdded;
            this.options.defaultButtonTextDefault = this.options.addToCartButtonTextDefault;
            this.options.defaultMiniSelector = this.options.minicartSelector;
        },
        /** @protected */
        _prepareForCartSubmit: function () {
            var self = this;
            this.element.attr('action', this.options.defaultAction);
            this.options.addToCartButtonSelector = this.options.defaultButtonSelector;
            this.options.addToCartButtonDisabledClass = this.options.defaultButtonDisabledClass;
            this.options.addToCartButtonTextWhileAdding = this.options.defaultButtonTextWhileAdding;
            this.options.addToCartButtonTextAdded = this.options.defaultButtonTextAdded;
            this.options.addToCartButtonTextDefault = this.options.defaultButtonTextDefault;
            this.options.minicartSelector = this.options.defaultMiniSelector;
        },
        /** @protected */
        _prepareForQuoteSubmit: function () {
            var self = this;
            this.element.attr('action', this.options.addToQuoteFormAction);
            this.options.addToCartButtonSelector = this.options.addToQuoteButtonSelector;
            this.options.addToCartButtonDisabledClass = this.options.addToQuoteButtonDisabledClass;
            this.options.addToCartButtonTextWhileAdding = this.options.addToQuoteButtonTextWhileAdding;
            this.options.addToCartButtonTextAdded = this.options.addToQuoteButtonTextAdded;
            this.options.addToCartButtonTextDefault = this.options.addToQuoteButtonTextDefault;
            this.options.minicartSelector = this.options.miniquoteSelector;
        },
        /** @protected */
        _init: function () {
            var self = this;
            if (this.element.data('quote-submit-action')) {
                this.options.addToQuoteFormAction = this.element.data('quote-submit-action');
            }
            this.element.find(this.options.addToQuoteButtonSelector).on(
                'click',
                this._prepareForQuoteSubmit.bind(this)
            );
            this.element.find(':submit').not(this.options.addToQuoteButtonSelector).on(
                'click',
                this._prepareForCartSubmit.bind(this)
            );
            var addToQuoteButton = this.element.find(this.options.addToQuoteButtonSelector);
            addToQuoteButton.removeClass(this.options.addToQuoteButtonDisabledClass);

            $(document).on('ajaxSend.catalog-add-product-cart', function (event, jqxhr, settings) {
                if (settings.url === self.options.defaultAction) {
                    $(document).trigger('startProductAddToCart');
                }
            });
            $(document).on('ajaxComplete.catalog-add-product-cart', function (event, jqxhr, settings) {
                if (settings.url === self.options.defaultAction) {
                    $(document).trigger('stopProductAddToCart');
                }
            });
            $(document).on('ajaxSuccess.catalog-add-product-cart', function (event, jqxhr, settings) {
                if (settings.url === self.options.defaultAction && !jqxhr.responseJSON.backUrl) {
                    $(document).trigger('successProductAddToCart');
                }
            });
            $(document).on('ajaxSend.catalog-add-product-quote', function (event, jqxhr, settings) {
                if (settings.url === self.options.addToQuoteFormAction) {
                    $(document).trigger('startProductAddToQuote');
                }
            });
            $(document).on('ajaxComplete.catalog-add-product-quote', function (event, jqxhr, settings) {
                if (settings.url === self.options.addToQuoteFormAction) {
                    $(document).trigger('stopProductAddToQuote');
                }
            });
            $(document).on('ajaxSuccess.catalog-add-product-quote', function (event, jqxhr, settings) {
                if (settings.url === self.options.addToQuoteFormAction && !jqxhr.responseJSON.backUrl) {
                    $(document).trigger('successProductAddToQuote');
                }
            });
        }
    });

    return $.cart2quote.catalogAddToCart;
});
