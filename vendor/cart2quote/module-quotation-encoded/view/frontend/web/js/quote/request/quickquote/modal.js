/*
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

define([
        'jquery',
        'Magento_Ui/js/modal/modal',
        'mage/translate',
        'Magento_Customer/js/action/login',
    ],
    function ($, modal, $t, loginAction) {
        "use strict";
        $.widget('mage.quickQuoteModal', $.mage.modal, {
            storageKey: 'open-modal-on-reload',
            storage: $.localStorage,
            options: {
                formSelector: '#quick-quote-form',
                wrapperClass: "quick-quote-wrapper",
                title: $t('Quick Quote'),
                buttons: [{
                    text: $t('Request Quote'),
                    class: "action primary requestquote",
                    attr: {
                        id: 'submit-quickquote-btn'
                    },
                    click: function () {
                        var form = this.element.find(this.options.formSelector);
                        if (form.valid()) {
                            $(this.element).trigger('processStart');
                            form.submit();
                        }
                    }
                }, {
                    text: $t('Continue shopping'),
                    class: "action secondary continueshopping",
                    click: function () {
                        this.closeModal();
                    }
                }]
            },
            _create: function () {
                this._super();
                var self = this;

                $(document).on('startProductAddToQuote', function () {
                    $(self.element).trigger('processStart');
                });

                $(document).on('successProductAddToQuote', function () {
                    self.openModal();
                    $(self.element).trigger('processStop');
                });

                if (self.storage.get(self.storageKey)) {
                    self.storage.set(self.storageKey, false);
                    self.openModal();
                }

                loginAction.registerLoginCallback(function () {
                    self.storage.set(self.storageKey, true);
                });
            },
        });
        return $.mage.quickQuoteModal;
    }
);