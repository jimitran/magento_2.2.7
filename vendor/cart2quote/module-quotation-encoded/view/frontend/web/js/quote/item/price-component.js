/*
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2019] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2019. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

define([
        'jquery',
        'uiComponent',
        'ko',
        'Magento_Customer/js/customer-data'
    ], function ($, Component, ko, customerData) {
        'use strict';
        return Component.extend({
            defaults: {
                price: null,
                itemId: null,
                template: 'Cart2Quote_Quotation/quote/item/price'
            },
            initialize: function () {
                this._super();
                customerData.get('quote').subscribe(this.updateItemPrice, this);
            },
            initObservable: function () {
                this.price = ko.observable(this.price);
                return this;
            },
            updateItemPrice: function (quote) {
                var self = this;
                quote.items.filter(function (item) {
                    if (item.item_id == self.itemId) {
                        self.price(item.product_price);
                    }
                });
            }
        });
    }
);