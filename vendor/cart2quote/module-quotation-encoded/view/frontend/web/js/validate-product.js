/*
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

define([
    'jquery'
], function ($) {
    'use strict';

    return function (widget) {
        $.widget('mage.productValidate', widget, {

            /**
             * Uses Magento's validation widget for the form object.
             * @private
             */
            _create: function () {
                var self = this;
                var bindSubmit = this.options.bindSubmit;
                var jqForm = $(this.element).catalogAddToCart(
                    $.extend(
                        {bindSubmit: bindSubmit},
                        self.options.catalogAddToCart
                    )
                );

                this.element.validation({
                    radioCheckboxClosest: this.options.radioCheckboxClosest,

                    /**
                     * Uses catalogAddToCart widget as submit handler.
                     * @param {Object} form
                     * @returns {Boolean}
                     */
                    submitHandler: function (form) {
                        jqForm.catalogAddToCart('submitForm', $(form));

                        return false;
                    }
                });
            }
        });
        return $.mage.productValidate;
    }
});