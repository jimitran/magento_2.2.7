<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Plugin\CustomerData;

use Magento\Framework\App\ObjectManager;

/**
 * Class DefaultItem
 * @package Cart2Quote\Quotation\Plugin\CustomerData
 */
class DefaultItem
{
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;
    /**
     * @var \Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface
     */
    private $itemResolver;

    /**
     * DefaultItem constructor.
     * @param \Magento\Catalog\Helper\Image $imageHelper
     */
    public function __construct(\Magento\Catalog\Helper\Image $imageHelper)
    {
        $this->imageHelper = $imageHelper;
    }

    /**
     * @param \Magento\Checkout\CustomerData\DefaultItem $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return array
     */
    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\DefaultItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item $item
    ) {
        $result = $proceed($item);
        $imageHelper = $this->imageHelper->init(
            $this->getProductForThumbnail($item),
            'cart_page_product_thumbnail'
        );

        return array_merge(
            $result,
            [
                'quickquote_product_image' => [
                    'src' => $imageHelper->getUrl(),
                    'alt' => $imageHelper->getLabel(),
                    'width' => $imageHelper->getWidth(),
                    'height' => $imageHelper->getHeight(),
                ],
            ]
        );
    }

    /**
     * @param $item
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    protected function getProductForThumbnail(\Magento\Quote\Model\Quote\Item $item)
    {
        if (class_exists(\Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface::class)) {
            $itemResolver = ObjectManager::getInstance()->get(
                \Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface::class
            );
            return $itemResolver->getFinalProduct($item);
        }

        return $item->getProduct();
    }
}