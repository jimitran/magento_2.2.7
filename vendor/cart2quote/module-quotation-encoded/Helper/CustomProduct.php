<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2019] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2019. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Helper;

use Magento\Quote\Model\Quote\Item;

/**
 * Class CustomProduct
 * @package Cart2Quote\Quotation\Helper
 */
class CustomProduct extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param Item $item
     * @return bool
     */
    public function isCustomProduct(Item $item){
        return $item->getSku() == \Cart2Quote\Quotation\Model\Quote\CustomProduct::SKU;
    }

    /**
     * @param Item $item
     * @return string|null
     */
    public function getCustomProductSku(Item $item){
        return $this->getItemOptionByLabel($item,'sku');
    }

    /**
     * @param Item $item
     * @return string|null
     */
    public function getCustomProductName(Item $item){
        return $this->getItemOptionByLabel($item,'name');
    }

    /**
     * @param Item $item
     * @param $label
     * @return string|null
     */
    private function getItemOptionByLabel(Item $item, $label){
        foreach ($this->getItemOptions($item) as $option) {
            if ($option['label'] === $label) {
                return $option['value'];
            }
        }
        return null;
    }

    /**
     * @param Item $item
     * @return array
     */
    public function getItemOptions(Item $item) {
        $result = [];
        $product = $item->getProduct();
        $options = $product->getTypeInstance()->getOrderOptions($product);
        if ($options) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }

        return $result;
    }
}