<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Block\Adminhtml\System\Config\Form\Field\Select;

use \Cart2Quote\Quotation\Model\Config\Source\Form\Field\Select\OptionInterface;

/**
 * Class OptionComments
 * @package Cart2Quote\Quotation\Block\Adminhtml\System\Config\Form\Field\Select
 */
class OptionComments extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _renderValue(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $sourceModel = \Magento\Framework\App\ObjectManager::getInstance()->get(
            $element->getFieldConfig()['source_model']
        );
        if ($sourceModel instanceof \Cart2Quote\Quotation\Model\Config\Source\Options) {
            $comment = $element->getComment();
            /**
             * @var OptionInterface $option
             */
            foreach ($sourceModel->getOptions() as $option) {
                $optionComment = $option->getComment();
                if (!empty($optionComment)) {
                    $comment .= $optionComment . '<br />';
                }
            }
            $element->setComment(rtrim($comment, '<br />'));
        }

        return parent::_renderValue($element);
    }
}