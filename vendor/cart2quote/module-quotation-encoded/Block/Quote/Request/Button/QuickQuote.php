<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Block\Quote\Request\Button;

/**
 * Class QuickQuote
 * @package Cart2Quote\Quotation\Block\Quote\Request\Button
 */
class QuickQuote extends Button implements \Cart2Quote\Quotation\Model\Strategy\StrategyInterface
{
    /**
     * @var string
     */
    protected $_template = "Cart2Quote_Quotation::product/view/quote/request/quickquote/button.phtml";

    /**
     * @var bool
     */
    private $modalIsRendered = false;

    /**
     * @param $modalBlockName
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getModalHtml($modalBlockName)
    {
        return $this->modalIsRendered ?: $this->getLayout()->getBlock($modalBlockName)->toHtml();
    }
}