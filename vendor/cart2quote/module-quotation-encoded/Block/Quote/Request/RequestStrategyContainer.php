<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Block\Quote\Request;

/**
 * Class RequestStrategyContainer
 * @package Cart2Quote\Quotation\Block\Quote\Request
 */
class RequestStrategyContainer extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * @var \Cart2Quote\Quotation\Block\Quote\Request\Provider
     */
    private $blockProvider;

    /**
     * @var string
     */
    private $blockChildAlias = 'addtoquote.button';

    /**
     * RequestStrategyContainer constructor.
     * @param \Cart2Quote\Quotation\Block\Quote\Request\Provider $blockProvider
     * @param \Magento\Framework\View\Element\Context $context
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\Quotation\Block\Quote\Request\Provider $blockProvider,
        \Magento\Framework\View\Element\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->blockProvider = $blockProvider;

        if (isset($data['blockChildAlias'])) {
            $this->blockChildAlias = $data['blockChildAlias'];
        }
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $blockChildAlias = $this->blockChildAlias;
        if (!$this->getChildBlock($blockChildAlias)) {
            $this->addChild($blockChildAlias, $this->blockProvider->getBlockClass());
        }

        return $this;
    }
}
