<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Block\Quote\Request;

/**
 * Class Provider
 * @package Cart2Quote\Quotation\Block\Quote\Request
 */
class Provider
{
    /**
     * @var \Cart2Quote\Quotation\Model\Strategy\Mapper
     */
    private $mapper;

    /**
     * Provider constructor.
     * @param \Cart2Quote\Quotation\Model\Strategy\Mapper $mapper
     */
    public function __construct(
        \Cart2Quote\Quotation\Model\Strategy\Mapper $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * @return \Cart2Quote\Quotation\Model\Strategy\StrategyInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function getBlockClass()
    {
        return $this->mapper->getMapping();
    }
}