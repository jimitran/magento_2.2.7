<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Block\Quote\QuoteCheckout;

use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;
use Magento\Customer\Helper\Address as AddressHelper;
use Magento\Customer\Model\Session;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Sales\Api\Data\OrderAddressInterface;

/**
 * Class AttributeMerger
 * @package Cart2Quote\Quotation\Block\Quote\QuoteCheckout
 */
class AttributeMerger extends \Magento\Checkout\Block\Checkout\AttributeMerger
{
    /**
     * @var AddressHelper
     */
    private $addressHelper;

    /**
     * AttributeMerger constructor.
     * @param AddressHelper $addressHelper
     * @param Session $customerSession
     * @param CustomerRepository $customerRepository
     * @param DirectoryHelper $directoryHelper
     */
    public function __construct(
        AddressHelper $addressHelper,
        Session $customerSession,
        CustomerRepository $customerRepository,
        DirectoryHelper $directoryHelper
    ) {
        $this->addressHelper = $addressHelper;

        parent::__construct(
            $addressHelper,
            $customerSession,
            $customerRepository,
            $directoryHelper
        );
    }

    /**
     * Check if address attribute is visible on frontend
     * Overwrite from parent to avoid the VAT is required error on quote request when frontend visibility is set to no.
     *
     * @param string $attributeCode
     * @param array $attributeConfig
     * @param array $additionalConfig field configuration provided via layout XML
     * @return bool
     */
    protected function isFieldVisible($attributeCode, array $attributeConfig, array $additionalConfig = [])
    {
        if ($attributeCode != OrderAddressInterface::VAT_ID) {
            return parent::isFieldVisible($attributeCode, $attributeConfig, $additionalConfig);
        } else {
            $taxVatVisible = $this->addressHelper->isVatAttributeVisible();
            $showTaxVat = (bool)$this->addressHelper->getConfig('taxvat_show');

            if (!$taxVatVisible && !$showTaxVat) {
                return false;
            }
        }

        return true;
    }
}
