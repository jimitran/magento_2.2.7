<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Block\Product\Listing;

/**
 * Class Form
 * @package Cart2Quote\Quotation\Block\Product\Listing
 */
class Form extends \Magento\Catalog\Block\Product\ProductList\Item\Block
{
    /**
     * @var \Cart2Quote\Quotation\Helper\Data
     */
    private $quotationHelper;

    /**
     * @var \Magento\Customer\Model\Session\Proxy
     */
    private $customerSession;

    /**
     * @var \Cart2Quote\Quotation\Helper\QuotationCart
     */
    private $quotationCartHelper;

    /**
     * Form constructor.
     * @param \Cart2Quote\Quotation\Helper\QuotationCart $quotationCartHelper
     * @param \Cart2Quote\Quotation\Helper\Data $quotationHelper
     * @param \Magento\Customer\Model\Session\Proxy $customerSession
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\Quotation\Helper\QuotationCart $quotationCartHelper,
        \Cart2Quote\Quotation\Helper\Data $quotationHelper,
        \Magento\Customer\Model\Session\Proxy $customerSession,
        \Magento\Catalog\Block\Product\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->quotationHelper = $quotationHelper;
        $this->customerSession = $customerSession;
        $this->quotationCartHelper = $quotationCartHelper;
    }

    /**
     * @return bool
     */
    public function showQuoteButton()
    {
        return $this->quotationHelper->showButtonOnList(
            $this->getProduct(),
            $this->customerSession->getCustomerGroupId()
        );
    }

    /**
     * @return bool
     */
    public function showMessageNotLoggedIn()
    {
        $configShowMessage = $this->_scopeConfig->getValue(
            'cart2quote_quotation/global/show_message_not_logged_in',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return $this->showQuoteButton() && !$this->customerSession->isLoggedIn() && $configShowMessage;
    }

    /**
     * @param $product
     * @param array $additional
     * @return string
     */
    public function getAddToQuoteUrl($product, $additional = [])
    {
        if (!$product->getTypeInstance()->isPossibleBuyFromList($product)) {
            /**
             * @var $parentBlock \Magento\Catalog\Block\Product\ListProduct
             */
            $parentBlock = $this->getParentBlock();
            $parentBlock->getAddToCartUrl($product, $additional);
        }

        return $this->quotationCartHelper->getAddUrl($product, $additional);
    }

    /**
     * @return bool|\Magento\Framework\View\Element\AbstractBlock
     */
    public function getParentBlock()
    {
        $parentBlock = parent::getParentBlock();
        if ($parentBlock instanceof \Cart2Quote\Quotation\Block\Quote\Request\RequestStrategyContainer) {
            return $parentBlock->getParentBlock();
        }

        return $parentBlock;
    }
}