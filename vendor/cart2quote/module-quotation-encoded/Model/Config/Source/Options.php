<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Model\Config\Source;

/**
 * Class Options
 * @package Cart2Quote\Quotation\Model\Config\Source\Quote
 */
class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    use \Cart2Quote\Features\Traits\Model\Config\Source\Options {
		toOptionArray as private traitToOptionArray;
		getOptions as private traitGetOptions;
	}

	/**
     * @var \Cart2Quote\Quotation\Model\Config\Source\Form\Field\Select\OptionInterface[]
     */
    private $options;

    /**
     * Strategies constructor.
     *
     * @param \Cart2Quote\Quotation\Model\Config\Source\Form\Field\Select\OptionInterface[] $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * Return array of options as value-label pairs, eg. value => label
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->traitToOptionArray();
    }

    /**
     * @return \Cart2Quote\Quotation\Model\Config\Source\Form\Field\Select\OptionInterface[]
     */
    public function getOptions()
    {
        return $this->traitGetOptions();
    }
}