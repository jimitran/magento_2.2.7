<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Model\Quote\Request\Strategy;

use Cart2Quote\Quotation\Model\Strategy\StrategyInterface;

/**
 * Class Provider
 * @package Cart2Quote\Quotation\Model\Quote\Strategy
 */
class Provider implements \Cart2Quote\Quotation\Model\Strategy\ProviderInterface
{
    use \Cart2Quote\Features\Traits\Model\Quote\Request\Strategy\Provider {
		getStrategy as private traitGetStrategy;
	}

	/**
     * system.xml path to quote request strategy
     */
    const XML_CONFIG_PATH_QUOTE_STRATEGY = 'cart2quote_quotation/global/quote_request_strategy';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var array
     */
    private $strategies;

    /**
     * Provider constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $strategies
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        $strategies = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->strategies = $strategies;
    }

    /**
     * @return StrategyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStrategy()
    {
        return $this->traitGetStrategy();
    }
}