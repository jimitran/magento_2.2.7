<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Model\Quote;

/**
 * Class CustomProduct
 * @package Cart2Quote\Quotation\Model\Quote
 */
class CustomProduct
{
    use \Cart2Quote\Features\Traits\Model\Quote\CustomProduct {
		createNewProduct as private traitCreateNewProduct;
		useExistingProduct as private traitUseExistingProduct;
	}

	/**
     * @const custom product sku
     */
    const SKU = 'custom-product';

    /**
     * CustomProduct constructor.
     * @param \Cart2Quote\Quotation\Helper\Data $dataHelper
     * @param \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     */
    public function __construct(
        \Cart2Quote\Quotation\Helper\Data $dataHelper,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Cart2Quote\Quotation\Model\Quote $quote
    ) {
        $this->quote = $quote;
        $this->dataHelper = $dataHelper;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
    }

    /**
     * @param array $params
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function createNewProduct($productParams)
    {
        return $this->traitCreateNewProduct($productParams);
    }

    /**
     * @param array $params
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function useExistingProduct($productParams)
    {
        return $this->traitUseExistingProduct($productParams);
    }
}
