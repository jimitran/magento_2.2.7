<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2019] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2019. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Quotation\Model\Quote\Email;

/**
 * Class UploadTransportBuilder
 * @package Cart2Quote\Quotation\Model\Quote\Email
 */
class UploadTransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{
    use \Cart2Quote\Features\Traits\Model\Quote\Email\UploadTransportBuilder {
		attachFile as private traitAttachFile;
		getMessage as private traitGetMessage;
		resetUploadTransportBuilder as private traitResetUploadTransportBuilder;
	}

	/**
     * @var \Cart2Quote\Quotation\Model\Email\ZendAdapter
     */
    private $zendAdapter;

    /**
     * UploadTransportBuilder constructor.
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param \Magento\Framework\Mail\Template\FactoryInterface $templateFactory
     * @param \Magento\Framework\Mail\MessageInterface $message
     * @param \Magento\Framework\Mail\Template\SenderResolverInterface $senderResolver
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Mail\TransportInterfaceFactory $mailTransportFactory
     * @param \Magento\Framework\Mail\MessageInterfaceFactory|null $messageFactory
     */
    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Magento\Framework\Mail\Template\FactoryInterface $templateFactory,
        \Magento\Framework\Mail\MessageInterface $message,
        \Magento\Framework\Mail\Template\SenderResolverInterface $senderResolver,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Mail\TransportInterfaceFactory $mailTransportFactory,
        \Magento\Framework\Mail\MessageInterfaceFactory $messageFactory = null
    ) {
        if (version_compare($productMetadata->getVersion(), 2.3) < 0) {
            parent::__construct(
                $templateFactory,
                $message,
                $senderResolver,
                $objectManager,
                $mailTransportFactory
            );
            $this->zendAdapter = $objectManager->get(\Cart2Quote\Quotation\Model\Email\ZendOne::class);
            $this->productMetadata = $productMetadata;
        } else {
            parent::__construct(
                $templateFactory,
                $message,
                $senderResolver,
                $objectManager,
                $mailTransportFactory,
                $messageFactory
            );
            $this->zendAdapter = $objectManager->get(\Cart2Quote\Quotation\Model\Email\ZendTwo::class);
        }
    }

    /**
     * Function to attach a file to an outgoing email
     * @param string $file
     * @param string $name
     * @return array
     */
    public function attachFile($file, $name)
    {
        return $this->traitAttachFile($file, $name);
    }

    /**
     * @param array $attachedPart
     * @return \Magento\Framework\Mail\TransportInterface
     */
    public function getMessage($attachedPart)
    {
        return $this->traitGetMessage($attachedPart);
    }

    /**
     * Reset UploadTransportBuilder object state
     *
     */
    public function resetUploadTransportBuilder()
    {
        $this->traitResetUploadTransportBuilder();
    }
}
