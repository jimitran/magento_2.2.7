# module-report-builder

Config sample:

```xml
<config>
    <table name="sales_order" connection="sales">
        <column name="triple_name" label="Client triple name" fields="customer_firstname"
                expr="CONCAT_WS('-', %1, %1, %1)"/>
    </table>
</config>
```
