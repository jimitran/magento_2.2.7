# Change Log
## 1.0.23
*(2019-01-15)*

#### Improvements
* Additional Columns

---


## 1.0.22
*(2019-01-08)*

#### Fixed
* LESS compilation issue (magento 2.1)  

---

## 1.0.21
*(2019-01-03)*

#### Fixed
* M2.1: issue during compilation
* Issue with building relations for prefixed tables

---

## 1.0.20
*(2018-12-19)*

#### Improvements
* Added relation for sales_order_grid

---

## 1.0.19
*(2018-12-04)*

#### Fixed
* Issue with link

---

## 1.0.18
*(2018-11-30)*

#### Fixed
* API

---

## 1.0.17
*(2018-11-12)*

---

## 1.0.16
*(2018-10-25)*

#### Improvements
* Allow to use PK, FK columns as dimensions and columns with type select as Fast Filters

#### Fixed
* Report builder always removes last column on report builder form
* Report Builder form is not opened on Magento 2.1.*

---

## 1.0.15
*(2018-09-27)*

#### Fixed
* Sorting columns

---

## 1.0.14
*(2018-09-26)*

#### Fixed
* Issue with error handling

---

## 1.0.13
*(2018-09-26)*

#### Improvements
* New Builder Interface

---

## 1.0.12
*(2018-09-21)*

#### Improvements
* Display explicit table relations

---

## 1.0.11
*(2018-06-04)*

#### Fixed
* Error displaying dashboard block settings due to wrong report added by report builder
* Issue with db table prefix during mapping

---

## 1.0.10
*(2018-05-16)*

#### Improvements
* Show correct table relation type in Report Builder settings

---

## 1.0.9
*(2018-05-10)*

#### Fixed
* Display all reports for Email Notification

---

## 1.0.8
*(2018-04-25)*

#### Improvements
* Refresh 'config' cache on creating/updating report config

---

## 1.0.7
*(2018-04-17)*

#### Improvements
* Compatibility with Magento 2.1

---

## 1.0.6
*(2018-04-10)*

#### Fixed
* Issue with Report Builder redirect

---

## 1.0.5
*(2018-04-05)*

#### Fixed
* Redirect while opening Report Builder

---

## 1.0.4
*(2018-04-04)*

#### Fixed
* Performance

---

## 1.0.3
*(2018-04-03)*

#### Fixed
* Error displaying/editing dashboard widgets for column type 'double'

---

## 1.0.2
*(2018-03-27)*

#### Improvements
* Ability to select chart type and columns

#### Fixed
* Error displaying/editing dashboard widgets for column type 'tinytext'
* Add acl for module resources

---

