<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rma
 * @version   2.0.38
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Rma\Block\Adminhtml\Rma\Create\Order\Column;

use Magento\Backend\Block\Context;
use Magento\Framework\DataObject;
use Mirasvit\Rma\Api\Service\Order\OrderManagementInterface;
use Mirasvit\Rma\Api\Service\Rma\RmaOrderInterface;

class OtherRmasColumn extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    private $orderManagementService;

    public function __construct(
        OrderManagementInterface $orderManagementService,
        RmaOrderInterface $rmaOrder,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->orderManagementService = $orderManagementService;
        $this->rmaOrder = $rmaOrder;
    }

    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $html = '';
        $order = new \Magento\Framework\DataObject();
        $order->setId($row['entity_id']);
        $rmas = $this->orderManagementService->getRmasByOrder($order);
        foreach ($rmas as $rma) {
            $order = $this->rmaOrder->getOrder($rma);
            if (!$order->getIsOffline()) {
                $url  = $this->getUrl('rma/rma/edit', ['id' => $rma->getId()]);
                $html .= '<a href="' . $url . '" target="_blank">#' . $rma->getIncrementId() . '</a><br/>';
            }
        }

        return $html;
    }
}