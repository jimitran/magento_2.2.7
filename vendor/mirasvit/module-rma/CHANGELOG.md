# Change Log

## 2.0.38
*(2019-02-27)*

#### Fixed
* Shipping method for RMA replacement order

---

## 2.0.37
*(2019-02-26)*

#### Fixed
* RMA customer name does not save for guest customer 

---

## 2.0.36
*(2019-02-19)*

#### Fixed
* Default storeview for guest RMA
* Issue when {{customer.name}} does not work for offline order 

---

## 2.0.35
*(2019-01-24)*

#### Fixed
* Replacement and Exchange order for guest RMA 

---

## 2.0.34
*(2019-01-21)*

#### Improvements
* Added report for Offline RMAs 

---

## 2.0.33
*(2019-01-16)*

#### Fixed
* Error "PHP 7.2. Warning: count(): Parameter must be an array or an object that implements Countable in ..."
* Display RMA for removed orders 

---

## 2.0.32
*(2019-01-04)*

#### Fixed
* Attribute Report does not show

---

## 2.0.31
*(2019-01-03)*

#### Fixed
* Issue errors for replacement order
* Issue when customer edit page does not work on Magento 2.1.x

---

## 2.0.30
*(2018-12-12)*

#### Fixed
* RMA "Created At" field updates on message send
* Issue when was possible to create RMA when all items were returned

#### Improvements
* Added other RMAs information on the backend pages

---

## 2.0.29
*(2018-11-29)*

#### Fixed
* Compatibility with Magento 2.3.0

---

### 2.0.27
*(2018-11-01)*

#### Fixed
* Custom statuses send messages for wrong storeview
* Styles for product image on RMA view page

#### Improvements
* Added Replacement order
* Added ability to assign Rma buttons (Exchange Order, Replacement Order and Credit Memo) to Resolution
* Added an RMA button on the order details page
* Added an RMAs section in the customer account in backend
* Added an RMA qty to the Items Ordered section of an order

---

### 2.0.26
*(2018-10-01)*

#### Fixed
* Issue when guest customer does not allow to post message
* Wrong product image in backend rma
* Wrong email sender

---

### 2.0.25
*(2018-09-13)*

#### Fixed
* Issue when guest customer does not allow to post message

---

### 2.0.24
*(2018-09-13)*

#### Fixed
* Stock amount does not show
* Renamed field in RMA guest login form
* Status does not show in emails

#### Improvements
* Fixed option "Force to apply styles"

---

### 2.0.23
*(2018-08-29)*

#### Improvements
* added RMA tab to backend order edit page

---

### 2.0.22
*(2018-08-02)*

#### Fixed
* RMA creation for bundle products
* Displaying of customer name on RMA backend edit page

---

### 2.0.21
*(2018-07-30)*

#### Fixed
* Issue when on status change changed owner for user message
* Unable to load RMA label in backend

---

### 2.0.20
*(2018-06-19)*

#### Features
* Added Offline Orders

#### Fixed
* Error "Undefined index: items"

---

### 2.0.19
*(2018-05-22)*

#### Fixed
* Email's variables
* Translation
* Duplicated styles

---

### 2.0.18
*(2018-04-13)*

#### Fixed
* Issue when customer is able to create two custom field with the same code
* Event "New reply from customer" does not work

#### Improvements
* Improved RMA guest page layout

---

### 2.0.17
*(2018-04-03)*

#### Fixed
* RMA frontend menu is not recognized as active
* Search in RMA select order grid

---

### 2.0.16
*(2018-03-26)*

#### Fixed
* Issue when removed exchange order cause fatal error

#### Improvements
* Added option that adds Rma style to pages if theme ignore it

---

### 2.0.15
*(2018-03-09)*

#### Fixed
* Support of new reports module

---

### 2.0.14
*(2018-03-05)*

#### Fixed
* Status changed by workflow rule does not send email notification

#### Improvements
* Added option "Show RMA for guest customer by order" displays only RMAs either for order, or for customer

---

### 2.0.13
*(2018-01-18)*

#### Improvements
* Added new variables(rma.getHasItemsWithReason(XXX), rma.getHasItemsWithConditions(XXX), rma.getHasItemsWithResolution(XXX)) for transactional emails, which allow to check existence of items with certain Reason, Resolution and Condition.

#### Fixed
* Added compatibility with Emizentech_CustomSmtp. Before this fix, if store had Emizentech_CustomSmtp, the option "Send blind carbon copy (BCC) of all emails to" was not working correctly.
* Fixed an issue with incorrect RMA URL. If a magento installation has stores with URL prefixes (e.g. "/usa/..", "/fr/.."), extension were not including those prefixes into RMA URLs.

---

### 2.0.12
*(2017-12-20)*

#### Fixed
* Extension registration fix
* Compilation crush fix

---

### 2.0.11
*(2017-11-15)*

#### Fixed
* Reports
* Compatibility with Magento 2.2.x

---

### 2.0.10
*(2017-11-13)*

#### Fixed
* Fixed images in backend

---

### 2.0.9
*(2017-11-08)*

#### Improvements
* RMA can be created for orders with deleted products

#### Fixed
* Report by Attribute
* Report by Product
* Product images in RMA creation form
* Exchange order filter in backend rma grid
* RMA urls
* Double emails on RMA creation

---

### 2.0.8
*(2017-10-06)*

#### Improvements
* Added reasons to print template

---

### 2.0.7
*(2017-10-04)*

#### Improvements
* Added "Resolved" to Workflow Rules actions

#### Fixed
* Email templates for multi store
* Customer notifications on status changes

---

# 2.0.6
*(2017-09-27)*

#### Fixed
* Compatibility with Magento 2.2.0

---

### 2.0.5
*(2017-09-26)*

#### Improvements
* Added ability to forbid products for return

### 2.0.4
*(2017-09-19)*

#### Fixed
* Bugs

---

### 2.0.3
*(2017-09-11)*

#### Improvements
* Reworked module documentation

---

### 2.0.2
*(2017-09-07)*

#### Improvements
* Improved admin UI

---

### 2.0.1
*(2017-09-07)*

#### Fixed
* Compatibility with Magento 2.2.0rc
* Displaying variations for configurable products

---

### 2.0.0

#### Improvements
* Interface refactored using XML UI Components
* Added mass action for change statuses in RMA Grid

---

### 1.1.26
*(2017-08-17)*

#### Improvements
* Added price column to items' grid in RMA form in backend
* Added mass action "Change Status" on RMA grid in backend

---

### 1.1.25
*(2017-07-11)*

#### Fixed
* Styles, misspelling and report filters

---

### 1.1.24
*(2017-06-09)*

#### Fixed
* RMA's reasons validation
* Dispaying status's messages

#### Improvements
* Added reports by product and by attribute

---

### 1.1.23
*(2017-05-31)*

#### Fixed
* RMA validation

#### Improvements
* Added return address on frontend

---

### 1.1.22
*(2017-05-10)*

#### Improvements
* Added option "Reset Counter For Order"
* Added "Ticket was converted to RMA" event

---

### 1.1.21
*(2017-04-26)*

#### Fixed
* Notifications for guest

---

### 1.1.20
*(2017-04-10)*

#### Fixed
* Configuration for multistore

#### Features
* Added new conditions to workflow rules

---

### 1.1.19
*(2017-04-05)*

#### Features
* Added reports by reasons

---

### 1.1.16
*(2017-04-04)*

#### Features
* Added option that allows request to return only shipped items

---

### 1.1.14
*(2017-03-31)*

#### Fixed
* Fixed RMA for partial shipment
* Fixed issue when admin does not get notification about new RMA

---

### 1.1.13
*(2017-03-30)*

#### Features
* Added Helpdesk integration

#### Fixed
* Fixed RMA for orders migrated from Magento 1

---

### 1.1.12
*(2017-03-13)*

#### Fixed
* Fixed auto selecting items

---

### 1.1.11
*(2017-03-09)*

#### Fixed
* Fixed styles for confirm shipping dialog

---

### 1.1.10
*(2017-03-07)*

#### Fixed
* Fixed a possible issue with store id

---

### 1.1.9
*(2017-03-03)*

#### Fixes
* Fixed translations for Resolutions, Conditions, Reasons
* Fixed option "Allow to request RMA after order completion"
* Fixed  issues with ability to delete reserved Resolutions

### 1.1.8
*(2017-02-28)*

#### Fixes
* Fixed compilation crush due to incomplete aggregation in reports

### 1.1.7
*(2017-02-27)*

#### Fixes
* Fixed compilation crush due to Reports requirement missing

### 1.1.6
*(2017-02-24)*

#### Features
* Added Report by RMA Status

---

### 1.1.5
*(2017-02-24)*

#### Improvements
* Added option to hide Condition, Resolution, Reason for customer

---

### 1.1.4
*(2017-02-15)*

#### Fixed
* Saving of translated dictionary (reasons, resolutions, conditions) titles for storeviews.

---

### 1.1.3
*(2017-01-13)*

#### Improvements
* Added ability to set different Return Addresses for each RMA

---

### 1.1.2
*(2017-01-11)* 

---

### 1.1.1
*(2016-12-21)*

#### Fixed
* Quick Response in RMA edit form
* Issue with merged js in production mode

---

### 1.1.0
*(2016-12-08)*

#### Fixed
* "Items" column in Rma grid

---

### 1.0.8
*(2016-06-24)*

#### Improvements
* Support of Magento 2.1.0

---

### 1.0.7
*(2016-05-06)*

#### Fixed
* Issue with sending emails via office.com smtp

---

### 1.0.6
*(2016-04-11)*

#### Fixed
* Issue with menu
* Fixed an issue with generation RMA increment ID

---

### 1.0.5
*(2016-03-24)*

#### Fixed
* hide button "Print RMA Shipping Label"

---

### 1.0.4
*(2016-03-21)*

#### Improvements
* Email styles

#### Fixed
* Compatibility with PHP7
* Compatibility of print templates with Proto theme

---

### 1.0.3
*(2016-03-07)*

#### Fixed
* Problem with shipping block on the RMA page in frontend

---

### 1.0.2
*(2016-03-03)*

#### Fixed
* RMA2-23 - Visible HTML tags in emails

---

### 1.0.1
*(2016-03-01)*

#### Improvements
* Backend and Frontend styles

#### Fixed
* RMA2-22 - Issues with PHP 7

---

### 1.0.0
*(2016-02-01)*

* initial stable release

------