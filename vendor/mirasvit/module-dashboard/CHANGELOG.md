# Change Log
## 1.2.39
*(2019-03-06)*

#### Fixed
* Issue with comparison (last year)

---


## 1.2.38
*(2019-02-25)*

#### Fixed
* Issue with overwrite dates

---


## 1.2.37
*(2019-01-31)*

#### Improvements
* Added comparison percent to email reports

---


## 1.2.36
*(2019-01-30)*

#### Improvements
* Comparison values to email reports

---


## 1.2.35
*(2018-12-27)*

#### Fixed
* Issue with metrics comparison

---


## 1.2.34
*(2018-12-18)*

#### Fixed
* Issue with email

---


## 1.2.33
*(2018-12-17)*

#### Improvements
* Charts block

---


## 1.2.32
*(2018-12-11)*

#### Fixed
* Issue with email reports filters

---


## 1.2.31
*(2018-12-06)*

#### Fixed
* Issue with select wrong dashboard for mobile view

---


## 1.2.30
*(2018-12-04)*

#### Improvements
* DateRange & Comparison

#### Fixed
* Overide date range

---


## 1.2.29
*(2018-11-30)*

#### Fixed
* API

---


## 1.2.28
*(2018-11-29)*

#### Improvements
* Major updates in UI

#### Fixed
* Compatibility with Magento 2.3
* Issue with Secret Key in Request url
* API
* Issue with table filtration

---

## 1.2.25
*(2018-10-12)*

#### Fixed
* Widget setting changes are not applied after saving

---

## 1.2.24
*(2018-10-10)*

#### Improvements
* Add like/not like conditions to widget settings

---

## 1.2.23
*(2018-10-02)*

#### Fixed
* Dashboard is not visible on mobile device

---

## 1.2.22
*(2018-09-11)*

#### Fixed
* Issue with reset recent changes

---

## 1.2.21
*(2018-09-10)*

#### Improvements
* Changed widgets position logic

---

## 1.2.20
*(2018-08-15)*

#### Fixed
* Cannot remove single dashboard

---

## 1.2.19
*(2018-07-26)*

#### Fixed
* Metrics do not correctly show values for today/yesterday dates

---

## 1.2.18
*(2018-07-25)*

#### Fixed
* Dashboard blocks with table renderer are not sent via emails

#### Improvements
* Add table renderer for dashboard block email

---

## 1.2.17
*(2018-07-24)*

#### Fixed
* Filter records by correct date column
* Wrong operator name for 'Is not one of' option in the dashboard block's filters

---

## 1.2.16
*(2018-06-22)*

#### Fixed
* Area code is not defined during installation

---

## 1.2.15
*(2018-06-19)*

#### Improvements
* Display only active report's columns

---

## 1.2.14
*(2018-06-15)*

#### Fixed
* Empty metric's data when editing old widgets

---

## 1.2.13
*(2018-06-14)*

#### Fixed
* Block settings are not shown: explicitly specify the component type option

---

## 1.2.12
*(2018-06-14)*

#### Features
* Filter and grid settings for dashboard blocks
* Add block level date comparison
* Different data depending on selected renderer

#### Improvements
* Second spark line for comparison
* Time icon for time-overridden blocks
* Persist previously selected report's columns for dashboard block

---

## 1.2.11
*(2018-05-02)*

#### Improvements
* Automatically update dashboard blocks every 2 minutes

#### Fixed
* Cannot create dashboard block

---

## 1.2.10
*(2018-04-17)*

#### Fixed
* Error opening dashboard

---

## 1.2.9
*(2018-04-16)*

#### Fixed
* Dashboard compatibility with Magento 2.1

---

## 1.2.8
*(2018-04-10)*

#### Fixed
* 'False' is displayed for option group instead of a table name at dashboard 'Data' field

---

## 1.2.7
*(2018-04-04)*

#### Fixed
* Error viewing dashboard, created_at and updated_at fields cannot be null

---

## 1.2.6
*(2018-03-30)*

#### Fixed
* Dashboard blocks of type 'table' are not displayed
* Do not ask to login when using QR code to look at the mobile dashboard

---

## 1.2.5
*(2018-03-20)*

#### Fixed
* Dashboard does not show information for one day date range

---

## 1.2.4
*(2018-03-16)*

#### Fixed
* Problems with date interval on dashboard toolbar

---

## 1.2.3
*(2018-03-07)*

#### Fixed
* issue with dates

---

## 1.2.2
*(2018-03-05)*

#### Fixed
* Attempt to change or create new dashboard gives an error

---

## 1.2.1
*(2018-02-13)*

#### Fixed
* issue with _asArray method

---

## 1.2.0
*(2018-02-05)*

### Improvements
* New version with many changes

---

### 1.0.18
*(2017-12-06)*

#### Fixed
* Fixed an issue with Dashboard controller

---

### 1.0.17
*(2017-11-07)*

#### Improvements
* Added the ability to create widgets for individual Store Views

---

### 1.0.16
*(2017-10-03)*

#### Fixed
* Compatibility with Magento 2.1.9

---

### 1.0.15
*(2017-08-22)*

#### Improvements
* Added abandoned cart metrics to "Metric" widgets

---

### 1.0.14
*(2017-08-07)*

#### Improvements
* Added support of Cart Reports widgets

---

### 1.0.13
*(2017-07-27)*

#### Improvements
* Added the ability to create "metrics" widgets for specific customer groups

#### Fixed
* Fixed formatting "price" type values in "Report" widgets

---

### 1.0.11
*(2017-06-07)*

#### Fixed
* Issue with widget edit

---

### 1.0.10
*(2017-06-07)*

#### Improvements
* Added an individual filter by order statuses to "metric" widgets

--- 

### 1.0.8
*(2017-06-06)*

#### Fixed
* Fixed display of product names in dashboard widgets
* Fixed an issue with filter by order statuses for "report" widgets

#### Improvements
* Applied the configuration "Process Orders" for metric widgets 

---

### 1.0.7
*(2017-05-12)*

#### Fixed
* Fixed an issue with using of attribute ID instead of the attribute label in the "Report" widget

---

### 1.0.6
*(2017-04-18)*

#### Fixed
* Fixed the issue with blank widgets in email notifications

---

### 1.0.5
*(2017-04-13)*

#### Fixed
* Solved an issue with duplication of rows in "Report" widgets

---

### 1.0.4
*(2017-04-05)*

#### Fixed
* Solved an issue with exception "No such widget"

---

### 1.0.3
*(2017-04-05)*

#### Features
* Ordered columns in the list in configuration of Report widget

#### Improvements
* Added initialization spinner to dashboard

---

### 1.0.2
*(2017-03-31)*

#### Improvements
* Mobile dashboard

#### Fixed
* [#3] -- error after removing widget

---

### 1.0.1
*(2017-02-20)*

#### Features
* Improved list of metrics in dropdown

#### Fixed
* Added widget types to widget Form
* Minor issues with js
* Fixed an issue with widgets saving
* Issue with menu

---

### 1.0.0
*(2017-01-31)*

#### Improvements
* Initial release

------