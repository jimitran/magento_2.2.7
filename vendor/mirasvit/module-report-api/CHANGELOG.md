## 1.0.22
*(2019-01-25)*

#### Fixed
* Sales order item join strategy

---

## 1.0.21
*(2019-01-18)*

#### Fixed
* Issue with wrong results for Sales by Attribute report

---

## 1.0.20
*(2019-01-02)*

#### Fixed
* Issue with sorting reports

---

## 1.0.19
*(2018-12-22)*

#### Improvements
* Multi-dimmension select

---

## 1.0.18
*(2018-12-17)*

#### Improvements
* Relations API

---

## 1.0.17
*(2018-12-11)*

#### Improvements
* Response interface

#### Fixed
* Issue with temporary tables

---

## 1.0.16
*(2018-12-05)*

#### Fixed
* Issue with timeout

---

## 1.0.15
*(2018-12-04)*

#### Fixed
* API

---

## 1.0.14
*(2018-11-29)*

#### Fixed
* Compatibility with Magento 2.3

#### Improvements
* Multi-dimension queries
* Use field name as default label for column name


---

## 1.0.12
*(2018-09-28)*

#### Improvements
* Ability to provide patches for creating the temporary tables

---

## 1.0.11
*(2018-09-26)*

#### Improvements
* Groups, Labels

---

## 1.0.10
*(2018-09-25)*

#### Improvements
* new column type 'serialized'

---

## 1.0.9
*(2018-09-21)*

#### Fixed
* Cannot create custom reports in some cases: due to deep level of relations

---

## 1.0.8
*(2018-09-05)*

#### Fixed
* Timezone is not applied in some reports, due to this some values are wrong
* Correctly show customer EAV attributes
* Reports are not displayed when attributes with absent source model exist

---

## 1.0.7
*(2018-08-17)*

#### Fixed
* 'Sales by Day of Week' report assigns wrong weekday name to a date

---

## 1.0.6
*(2018-08-03)*

#### Improvements
* Ability to count columns of type QTY

---

## 1.0.5
*(2018-08-03)*

#### Improvements
* Ability to count columns of type String

---

## 1.0.4
*(2018-07-30)*

#### Fixed
* Count only distinct values

---

## 1.0.3
*(2018-07-30)*

#### Improvements
* API method to check whether the column is filter only

---

## 1.0.2
*(2018-07-24)*

#### Fixed
* Filter is not applied in some cases

---

## 1.0.1
*(2018-07-23)*

#### Improvements
* Translate column's names

---