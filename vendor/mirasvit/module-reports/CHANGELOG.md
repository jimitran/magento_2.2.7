# Change Log
## 1.3.30
*(2019-03-01)*

#### Improvements
* Minor changes

---
## 1.3.29
*(2019-01-25)*

#### Improvements
* Sales Order Item Strategy

---


## 1.3.27
*(2018-12-20)*

#### Fixed
* Email Reports

---


## 1.3.25
*(2018-11-29)*

#### Improvements
* Major changes in UI

#### Fixed
* Compatibility with Magento 2.3

---


## 1.3.24
*(2018-10-26)*

#### Fixed
* 'This report no longer exists' error after saving report settings

---

## 1.3.23
*(2018-10-12)*

#### Fixed
* Sales by attribute shows zero totals for some attributes

#### Documentation
* new table attribute 'group'

---


## 1.3.22
*(2018-10-02)*

#### Fixed
* Catalog Attribute report shows wrong values (affects since 1.3.21)

---

## 1.3.21
*(2018-09-28)*

#### Fixed
* Sales by Attribute report shows 0 for simple items grouped by configurable attributes
* Do not display 'View Customer' action in Abandoned Carts report for records without registered customer

---

## 1.3.20
*(2018-09-17)*

#### Fixed
* Abandoned Products report does not properly show data

---

## 1.3.19
*(2018-09-04)*

#### Improvements
* Add order increment IDs column to product detailed report
* Add Order Payment columns to Sales Overview report

#### Fixed
* Show number of unknown postcodes in community edition
* Correct link to mst_report.xml schema validation file

#### Documentation
* How to create Low Stock report, widget and email notification

---

## 1.3.18
*(2018-08-13)*

#### Improvements
* Add order items columns to Product Performance report
* Use shipping address state for Geo report as the fallback, if state has not been downloaded

#### Documentation
* Troubleshoot for case when region part is not highlighted on a map in the Sales by Geo-data report

---

## 1.3.17
*(2018-08-10)*

#### Improvements
* Do not log 'No valid keys' error
* Use free daily quote instead of keys for downloading info about unknown postcodes

#### Documentation
* How to build table relationship using Config Builder tool
* How to translate column's names

---

## 1.3.16
*(2018-08-03)*

#### Improvements
* Add Order Item columns to Sales reports

#### Documentation
* No valid keys troubleshoot

---

## 1.3.15
*(2018-07-27)*

#### Fixed
* Product Performance detailed report does not filter by selected product

---

## 1.3.14
*(2018-07-24)*

#### Fixed
* Update API codes for fetching geodata and update unknown post codes

---

## 1.3.13
*(2018-07-19)*

#### Improvements
* Add customer group filter to Product Performance report

#### Fixed
* Report emails sent multiple times

---

## 1.3.12
*(2018-07-13)*

#### Fixed
* Report New vs Returning Customers: treat guest customers as new

#### Documentation
* Change admin panel startup page

---

## 1.3.11
*(2018-07-02)*

#### Improvements
* Add order status filter to Sales by Category and Sales by Attribute reports

---

## 1.3.10
*(2018-06-21)*

#### Fixed
* Correctly display 'Total Cost' value at Product Performance report

---

## 1.3.9
*(2018-06-19)*

#### Fixed
* Report Sales by Geo-data is not filtered by date

---

## 1.3.8
*(2018-06-19)*

#### Improvements
* ACL permissions for report settings

---

## 1.3.7
*(2018-06-06)*

#### Improvements
* Add Payment Method filter to Sales Overview report

#### Documentation
* Information about totals in custom reports
* How to add attribute column to custom report with Report Builder

---

## 1.3.6
*(2018-06-04)*

#### Fixed
* Issue with Total Cost
* Issue with filtration by store view
* Postcodes

---

## 1.3.5
*(2018-04-25)*

#### Improvements
* Additional columns for Sales Overview and Sales by Attribute reports

---

## 1.3.4
*(2018-04-17)*

#### Fixed
* Error displaying abandoned cart detailed report

---

## 1.3.3
*(2018-04-17)*

#### Fixed
* Error during compilation: remove extra dependency
* Compatibility with Magento 2.1

---

## 1.3.2
*(2018-04-05)*

#### Fixed
* Detailed product report opened from Product Performance report does not display information for specific product

---

## 1.3.1
*(2018-02-16)*

#### Features
* Report Builder

#### Documentation
* Report Builder documentation

---

## 1.3.0
*(2018-02-09)*

#### Improvements
* Geo Report

#### Fixed
* Report Sales By Customer
* Added Managing of Geo Data menu item

---

### 1.1.35
*(2017-12-07)*

#### Improvements
* Added filters by "Customer Group", "Order Status", "Day of Week of purchase" to "Sales by Attribute" and "Sales by Attribute Set" reports

---

### 1.1.34
*(2017-12-07)*

#### Fixed
* filters by "Customers > Products" and "Abandoned Carts > Abandoned Products" columns

---

### 1.1.33
*(2017-12-06)*

#### Fixed
* Filter by "Products" column

---

### 1.1.32
*(2017-12-04)*

#### Improvements
* Added "Product Total Margin" field

---

### 1.1.31
*(2017-12-04)*

#### Fixed
* API urls for post codes
* Added "mirasvit:reports:test" console command

---

### 1.1.30
*(2017-11-13)*

#### Features
* Added "Customers" report

---

### 1.1.29
*(2017-10-26)*

#### Fixed
* Updated connection for quote tables

#### Improvements
* Added "Tax Title", "Tax Percent" to Orders report

---

### 1.1.28
*(2017-10-24)*

#### Features
* Added "Sales by Attribute Set" report

---

### 1.1.27
*(2017-10-17)*

#### Improvements
* Added column "Gross Margin" to "Product Performance" report

---

### 1.1.26
*(2017-10-17)*

---

### 1.1.25
*(2017-10-17)*

#### Improvements
* Added column "Invoice ID" to Sales > Orders report
* Added "Grand Total excluding Tax" column to the Sales reports

---

### 1.1.24
*(2017-10-02)*

---

### 1.1.23
*(2017-09-27)*

#### Fixed
* Compatibility with Magento 2.2

---

### 1.1.22
*(2017-09-18)*

#### Fixed
* Fix cron error 'undefined property' when executing 'reports_postcode_update' cron job
* Compatibility with Magento 2.2.0rc

---

### 1.1.21
*(2017-08-22)*

#### Improvements
* Added Transaction ID and Type to Orders report
* Added "customer group" column to Abandoned Cart reports

#### Fixed
* Fixed the hints for quarters when building a quarterly report. 
* Fixed the issue with duplication in "Qty Refunded" column
* Corrected the calculation of the "out of stock estimate" for the case of a negative quantity of product in inventory

---

### 1.1.20
*(2017-08-07)*

#### Features
* Added "Abandoned Products" report
* Added "Abandoned Prodacts / Detail" report

#### Improvements
* Added chart to "Product Performance Detail" report

---

### 1.1.19
*(2017-08-02)*

#### Fixed
* Fixed accounting of "active" abandoned carts

---

### 1.1.18
*(2017-08-02)*

#### Fixed
* Fixed requirements for compatibility with the mirasvit/module-report

---

### 1.1.17
*(2017-08-02)*

#### Features
* Added "Abandoned Carts Overview" report
* Added "Abandoned Carts" report

#### Improvements
* Improved reports menu view

---

### 1.1.16
*(2017-08-01)*

#### Features
* Added "Sales by Cart Price Rules" report
* Added "Sales by Tax Rates" report

#### Improvements
* Added ability to view the sold products from "Sales Overview" report

---

### 1.1.15
*(2017-07-11)*

#### Improvements
* Added column "QTY refunded" to Sales Overview report

---

### 1.1.14
*(2017-07-11)*

#### Improvements
* Added 2 columns "Out of Stock Estimation Date" and "Stock Qty"

---

### 1.1.13
*(2017-07-11)*

#### Improvements
* New Charts

---

### 1.1.12
*(2017-06-27)*

#### Features
* Modified the method of using Google geocoding

---

### 1.1.11
*(2017-06-21)*

#### Improvements
* Refactoring

---

### 1.1.10
*(2017-06-15)*

#### Fixed
* Fixed an issue with default filter in "Sales by Attribute" and "Sales by Category" reports

---

### 1.1.9
*(2017-06-08)*

#### Fixed
* An issue with DaysOfWeek column

---

### 1.1.8
*(2017-06-07)*

#### Improvements
* Removed global limitation by order status

---

### 1.1.7
*(2017-05-31)*

#### Fixed
* Issue with duplication at "Product Performance" report

---

### 1.1.6
*(2017-05-30)*

#### Features
* Added filters to Manage Geo Data Grid

--- 

### 1.1.5
*(2017-05-30)*

#### Fixed
* Issue with geo redeclaration default UI

---

### 1.1.4
*(2017-05-29)*

#### Features
* Added possibility to delete Geo data from admin panel

#### Fixed
* Fixed an issue of obtaining and duplication of Geo data in different languages

---

### 1.1.2
*(2017-05-17)*

#### Fixed
* Fixed Email Notification ACL

---

### 1.1.1
*(2017-05-04)*

#### Fixed
* Fixed the issue with Gross Profit in the case when the cost of product is not defined
* Fixed issue with cron job with Email Notifications

---

### 1.1.0
*(2017-02-20)*

#### Fixed
* Pool and dateHelper at email notification
* Pool at email notification

---

### 1.0.19
*(2017-01-09)*

#### Improvements
* Ability to filter by order status
* GEO

---

### 1.0.18.1
*(2016-12-23)*

#### Fixed
* Fixed an issue with cronjob

---

### 1.0.18
*(2016-12-22)*

#### Improvements
* Improved geo-data fetching logic

---

### 1.0.17
*(2016-12-15)*

#### Improvements
* Base geo reports

#### Fixed
* Fixed an issue with date ranges

---

### 1.0.16
*(2016-09-20)*

---

### 1.0.14
*(2016-07-28)*

#### Fixed
* Fixed an issue with report Product Performance - Detail

---

### 1.0.13
*(2016-07-27)*

#### Fixed
* Fixed an issue with wrong product cost calculations

---

### 1.0.12
*(2016-07-01)*

#### Fixed
* Renamed report.xml to mreport.xml (compatibility with module-support)

---

### 1.0.11
*(2016-06-24)*

#### Fixed
* Compatibility with Magento 2.1

#### Documentation
* Updated installation instructions

---

### 1.0.9, 1.0.10
*(2016-05-31)*

#### Fixed
* Fixed an issue with building reports based on all orders (filteration by status are missed)
* Fixed an issue with wrong requirements

---

### 1.0.8
*(2016-05-27)*

#### Improvements
* Added ability to filter all reports by store

---

### 1.0.7
*(2016-05-25)*

#### Fixed
* Fixed permissions issue

---

### 1.0.6
*(2016-05-19)*

#### Improvements
* Added Products column to Orders report

---

### 1.0.5
*(2016-04-11)*

#### Improvements
* Integration tests
* Refactoring

#### Fixed
* Fixed an issue with menu
* Minor fixes
* Fixed an issue with filtration

#### Documentation
* Updated installation steps

---

### 1.0.4
*(2016-02-29)*

#### Fixed
* Fixed an issue with wrong requirejs declaration
* Fixed an issue with percent calculation
* Fixed an issue with applying the filters
* Fixed an issue with di compilation
* Removed geo export console command

---

### 1.0.3
*(2016-02-02)*

#### Improvements
* Update required core version to 1.2

---
